import requests
import itertools
from collections import Counter, namedtuple

import pandas as pd
import numpy as np

from sklearn.preprocessing import normalize
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score


API_ROOT = 'http://ws.audioscrobbler.com/2.0/'
# API_KEY = 'YOUR_LASTFM_API_KEY'

Track = namedtuple('Track', ['mbid', 'title', 'artist'])


def get_unique_recent_tracks(username, limit):
    r = requests.get(API_ROOT + '?method=user.getrecenttracks' + 
                     '&user=' + username +
                     '&api_key=' + API_KEY + 
                     '&limit=' + str(limit) + '&format=json')
    data = r.json()
    tracks = [Track(track['mbid'], track['name'], track['artist']['#text']) for track in data['recenttracks']['track']]
    tracks = list(set(filter(lambda t: t[0], tracks)))
    return tracks


def get_acousticbrainz_data(tracks):
    songs = []
    new_tracks = []  
    index = None
    
    for track in tracks:
        r = requests.get('https://acousticbrainz.org/api/v1/'+ track.mbid +'/high-level')
        if r.ok:
            data = r.json()['highlevel']
            if index is None:
                tuples = [(outer, inner) for outer in data for inner in data[outer]['all']]
                index = pd.MultiIndex.from_tuples(tuples)
            
            values = [data[outer]['all'][inner] for outer in data for inner in data[outer]['all']]
            s = pd.Series(values, index=index)
            songs.append(s)
            new_tracks.append(track)
    return pd.DataFrame(songs, index=new_tracks)


def drop_correlated(dataframe, threshold=0.9):
    corr_matrix = dataframe.corr().abs()   
    upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))  
    to_drop = [column for column in upper.columns if any(upper[column] > threshold)]
    
    return dataframe.drop(to_drop, axis=1)


def classify(dataframe, groups_min=2, groups_max=10, pca_comp=20):
    dataframe = (dataframe-dataframe.mean())/dataframe.std()
    
    pca = PCA(n_components=min(len(dataframe.columns), pca_comp))
    pca.fit(dataframe)
    pts = pca.transform(dataframe)
    
    sils = []
    
    for n in range(groups_min, groups_max + 1):
        kmeans = KMeans(n_clusters=n, random_state=0).fit(pts)
        sils.append((silhouette_score(pts, kmeans.labels_), n))
    
    n = sils[np.argmax([sil[0] for sil in sils])][1]
    kmeans = KMeans(n_clusters=n, random_state=0).fit(pts)
    
    return sorted([song + (label,) for song, label in zip(dataframe.index, kmeans.labels_)], key=lambda x: x[3])


def get_tags(songs):
    tags = []
    
    for song in songs:
        r = requests.get(API_ROOT + '?method=track.getInfo' +
                         '&mbid=' + song[0] +
                         '&api_key=' + API_KEY + '&format=json')
        tags.append(r.json())
    
    tags = [[i['name'] for i in t['track']['toptags']['tag'][:3]] for t in tags]
    return tags


def get_playlists(songs, tags):
    groups = []
    
    for k, g in itertools.groupby(zip(songs, tags), key=lambda x: x[0][3]):
        groups.append(list(g))
    
    flatten = lambda l: [item for sublist in l for item in sublist]
    playlists = []
    
    for group in groups:
        words = flatten([item[1] for item in group])
        c = Counter(words)
        
        playlist = []
        for song in group:
            playlist.append(song[0][:3][::-1])
        args = np.argsort(-np.array(list(c.values())))[:3]
        keys = list(c.keys())
        playlists.append({'tracks': playlist, 'tags': [keys[i] for i in args]})
    return playlists
