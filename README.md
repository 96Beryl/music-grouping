# Music Grouping
The project is a little bit rough on the edges, requires some more error handling, probably some refactoring, but it works and you can see for yourself how it is performing :)

## Setup

You just need to put your api key in the grouping.py file.

---

## Running

Run flask_server.py script. The webpage is at the address localhost:5000.
