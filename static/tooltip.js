function floatingTooltip(width) {
  var tooltip = d3.select('body')
                  .append('div')
                  .attr('class', 'tooltip')
                  .style('pointer-events', 'none');

  if (width) {
    tooltip.style('width', width);
  }

  hideTooltip();

  function showTooltip(content, event) {
    tooltip.style('opacity', 1.0)
           .html(content);

           updatePosition(event);
  }

  function hideTooltip() {
    tooltip.style('opacity', 0.0);
  }

  function updatePosition(event) {
    var xOffset = 20;
    var yOffset = 10;

    var width = tooltip.style('width');
    var height = tooltip.style('height');

    var windowScrollY = window.scrollY;
    var windowScrollX = window.scrollX;

    var cursorX = (document.all) ? event.clientX + windowScrollX : event.pageX;
    var cursorY = (document.all) ? event.clientY + windowScrollY : event.pageY;
    var tooltipLeft = ((cursorX - windowScrollX + xOffset * 2 + width) > window.innerWidth) ?
                 cursorX - width - xOffset * 2 : cursorX + xOffset;

    if (tooltipLeft < windowScrollX + xOffset) {
      tooltipLeft = windowScrollX + xOffset;
    }

    var tooltipTop = ((cursorY - windowScrollY + yOffset * 2 + height) > window.innerHeight) ?
                cursorY - height - yOffset * 2 : cursorY + yOffset;

    if (tooltipTop < windowScrollY + yOffset) {
      tooltipTop = cursorY + yOffset;
    }

    tooltip.style({ top: tooltipTop + 'px', left: tooltipLeft + 'px' });
  }

  return {
    showTooltip: showTooltip,
    hideTooltip: hideTooltip,
    updatePosition: updatePosition
  };
}
