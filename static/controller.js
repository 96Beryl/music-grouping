$("#user-form").submit(function(e) {
    e.preventDefault(); // prevent executing the actual submit of the form.

    d3.selectAll('.error').remove()
    showLoader();
    showingLists = false;
    myBubbleChart.hideLists();

    var username = $("#username").val();
    var nSongs = $("#n-songs").val();
    var url = "http://localhost:5000/playlists/" + username + "/" + nSongs;

    $.ajax({
           type: "GET",
           url: url,
           success: function(data) {
             handleSuccess(data);
           },
           error: function(data) {
             handleError(data);
           }
         });
});

function handleSuccess(data) {
    $('#loader').hide();
    myBubbleChart('#visualization', data.playlists);

    d3.select('.chart-controls')
      .selectAll('button')
      .classed('active', false);
    d3.select('#single')
      .classed('active', true)

}

function handleError(data) {
    $('#loader').hide();
    d3.select('#visualization')
      .append('span')
      .attr('class', 'error')
      .text('Something was wrong with your request. Please change it (check if the user exists or try more songs) and try again or try again later.')
}

function showLoader() {
  d3.select('svg').remove();
  $('#loader').show();
}

function toggleList() {
  showingLists = !showingLists;
  if(showingLists) {
    myBubbleChart.showList();
  } else {
    myBubbleChart.hideLists();
  }
}

function startDownload() {
  d3.select('.list-controls')
    .append('input')
    .attr('class', 'download')
    .attr('id', 'filename')
    .attr('type', 'text')
  d3.select('.list-controls')
    .append('span')
    .attr('class', 'download')
    .text('.txt')
  d3.select('.list-controls')
    .append('button')
    .attr('class', 'btn btn-info download')
    .text('Download')
    .on('click', download)
}

function download() {
  var filename = $('#filename').val();
  d3.selectAll('.download').remove();
  myBubbleChart.download(filename);
}

function setupChartControls() {
  d3.select('.chart-controls')
    .selectAll('button')
    .on('click', function () {
      d3.select('.chart-controls').selectAll('button').classed('active', false);
      var button = d3.select(this);
      button.classed('active', true);
      var buttonId = button.attr('id');
      myBubbleChart.toggleDisplay(buttonId);
    });
}

function setupListControls() {
  d3.select('#toggle-list')
    .on('click', toggleList)
  d3.select('#download')
    .on('click', startDownload)
}

var showingLists = false;
var myBubbleChart = bubbleChart();
setupChartControls();
setupListControls();
