function bubbleChart () {
    var height = 600;
    var width = 800;
    var maxRadius = 60;
    var minRadius = 5;
    var radiusVar = 3;
    var center = { x: width / 2, y: height / 2 };
    var playlists = {};
    var playlistsTags = {};
    var damper =  0.102;
    var displayMode = 'single'

    var tooltip = floatingTooltip(240);

    var svg = null;
    var bubbles = null;
    var nodes = [];

    function charge(d) {
      return -Math.pow(d.radius, 2.0) / 8;
    }

    var force = d3.layout.force()
       .size([width, height])
       .charge(charge)
       .gravity(-0.01)
       .friction(0.9);

    function createPlaylistsCoords(n) {
      var nCols = Math.ceil(Math.sqrt(n));
      var nRows = Math.ceil(n / nCols);
      var i = 0, r = 0, c = 0;
      while(i < n) {
        if(c >= nCols) {
          c = 0;
          r++;
        }
        if(r == nRows - 1 && n % nCols != 0) {
          playlists[i] = {x: width / (n % nCols + 1) * (c + 1), y: height / (nRows + 1) * (r + 1)};
        } else {
          playlists[i] = {x: width / (nCols + 1) * (c + 1), y: height / (nRows + 1) * (r + 1)};
        }
        c++;
        i++;
      }
    }

    function calcRadius(n) {
      return 0.001 * (n-200) ^ 2 + 5;
    }

    function createNodes(data) {
      var songs = [];
      for(var i = 0; i < data.length; i++) {
        playlist = data[i];
        playlistsTags[i] = playlist.tags;
        for(var j = 0; j < playlist.tracks.length; j++) {
          var track = playlist.tracks[j];
          var song = {id: songs.length, playlistId: i, artist: track[0], title: track[1], musicBrainzId: track[2]};
          songs.push(song);
        }
      }
      createPlaylistsCoords(data.length);
      var n = songs.length;
      var baseRadius = calcRadius(n);
      var nodes = songs.map(function (d) {
        return {
          id: d.id,
          playlistId: d.playlistId,
          radius: baseRadius + (Math.random() * 2 - 1) * radiusVar,
          artist: d.artist,
          title: d.title,
          musicBrainzId: d.musicBrainzId,
          x: Math.random() * width,
          y: Math.random() * height
        };
      });

      nodes.sort(function (a, b) { return b.radius - a.radius; });
      return nodes;
    }

    function randomColor(seed) {
      // 16777215 == ffffff in decimal
      Math.seedrandom(seed.toString())
      var color = '#'+Math.floor(Math.random() * 16777215).toString(16);
      return color;
    }

    var chart = function chart(domElement, data) {
      playlists = {};
      playlistsTags = {};

      svg = d3.select(domElement)
        .append('svg')
        .attr('width', width)
        .attr('height', height);

      nodes = createNodes(data);

      force.nodes(nodes);

      bubbles = svg.selectAll('.bubble')
        .data(nodes, function (d) { return d.id; });

      bubbles.enter().append('a')
        .attr('xlink:href', function(d) {return 'https://musicbrainz.org/recording/' + d.musicBrainzId;})
        .attr("xlink:target", "_blank")
        .append('circle')
        .classed('bubble', true)
        .attr('r', 0)
        .attr('stroke-width', 2)
        .on('mouseover', showDetail)
        .on('mouseout', hideDetail);

      bubbles.selectAll('.bubble')
        .transition()
        .duration(2000)
        .attr('r', function (d) { return d.radius; });

      groupBubbles();
    }

    function showDetail(d) {
      d3.select(this).attr('stroke', 'black');

      var content = '<span class="name">Artist: </span><span class="value">' +
                    d.artist +
                    '</span></br>' +
                    '<span class="name">Title: </span><span class="value">' +
                    d.title +
                    '</span>';
      tooltip.showTooltip(content, d3.event);
    }

    function hideDetail(d) {
      if(displayMode == 'group') {
        d3.select(this)
          .attr('stroke', d3.rgb(randomColor(d.playlistId)).darker());
      } else {
        d3.select(this)
          .attr('stroke', d3.rgb(randomColor(d.id)).darker());
      }

      tooltip.hideTooltip();
    }

    function showPlaylistDetail(d) {
      d3.select(this).attr('stroke', 'black');
      var tags = playlistsTags[d];

      var content = '<span class="name">Tags: </span><span class="value">';
      tags.forEach(function(tag) {content += '<span class="tag">' + tag + '</span>';})
      content += '</span>';
      tooltip.showTooltip(content, d3.event);
    }

    function hidePlaylistDetail(d) {
      d3.select(this).attr('stroke', 'none');
      tooltip.hideTooltip();
    }

    function groupBubbles() {
      var bubbles = d3.select('svg')
        .selectAll('.bubble')
        .attr('fill', function (d) { return randomColor(d.id); })
        .attr('stroke', function (d) { return d3.rgb(randomColor(d.id)).darker(); })

      force.on('tick', function (e) {
        bubbles.each(moveToCenter(e.alpha))
            .attr('cx', function (d) { return d.x; })
            .attr('cy', function (d) { return d.y; });
      });

      force.start();
    }

    function moveToCenter(alpha) {
      return function (d) {
        d.x = d.x + (center.x - d.x) * damper * alpha;
        d.y = d.y + (center.y - d.y) * damper * alpha;
      };
    }

    function splitBubbles() {
      var bubbles = d3.select('svg')
        .selectAll('.bubble')
        .attr('fill', function (d) { return randomColor(d.playlistId); })
        .attr('stroke', function (d) { return d3.rgb(randomColor(d.playlistId)).darker(); })

      force.on('tick', function (e) {
        bubbles.each(moveToPlaylists(e.alpha))
          .attr('cx', function (d) { return d.x; })
          .attr('cy', function (d) { return d.y; });
      });

      force.start();
    }

    function moveToPlaylists(alpha) {
      return function (d) {
        var target = playlists[d.playlistId];
        d.x = d.x + (target.x - d.x) * damper * alpha * 1.1;
        d.y = d.y + (target.y - d.y) * damper * alpha * 1.1;
      };
    }

    function hidePlaylists() {
      svg.selectAll('.playlist').remove();
    }

    function showPlaylists() {
      var playlistsData = d3.keys(playlists);
      var labels = svg.selectAll('.playlist')
        .data(playlistsData);

      labels.enter().append('text')
        .attr('class', 'playlist')
        .attr('x', function (d) { return playlists[d].x; })
        .attr('y', function (d) { return playlists[d].y - 60; })
        .attr('text-anchor', 'middle')
        .text(function (d) { return "Playlist " + (parseInt(d) + 1); })
        .on('mouseover', showPlaylistDetail)
        .on('mouseout', hidePlaylistDetail);
    }

    chart.toggleDisplay = function (displayId) {
      displayMode = displayId;
      if (displayMode === 'group') {
        showPlaylists();
        splitBubbles();
      } else {
        hidePlaylists();
        groupBubbles();
      }
    }

    chart.hideLists = function() {
      d3.selectAll('table').remove()
    }

    chart.showList = function () {
      var lists = {};
      var data = svg.selectAll('.bubble')
        .data();
      data.forEach(function(song) {
        if(lists[song.playlistId] == null) {
          lists[song.playlistId] = [song];
        } else {
          lists[song.playlistId].push(song);
        }
      });

      var columns = ['Artist', 'Title']

      var container = d3.select('.playlist-list')

      for(var key in lists) {
        var table = container.append('table').attr('class', 'playlist-table');
        var tcaption = table.append('caption').text("Playlist " + (parseInt(key)+1) + " Tags: " + playlistsTags[key].toString().replaceAll(',', ' '));
    		var thead = table.append('thead')
    		var	tbody = table.append('tbody');


    		thead.append('tr')
    		  .selectAll('th')
    		  .data(columns).enter()
    		  .append('th')
    		    .text(function (d) { return d; });

    		var rows = tbody.selectAll('tr')
    		  .data(lists[key])
    		  .enter()
    		  .append('tr');

    		var cells = rows.selectAll('td')
    		  .data(function (row) {
    		    return columns.map(function (column) {
    		      return {column: column, value: row[column.toLowerCase()]};
    		    });
    		  })
    		  .enter()
    		  .append('td')
    		    .text(function (d) { return d.value; });
      }

    }

    chart.download = function(filename) {
      var lists = {};
      var data = svg.selectAll('.bubble')
        .data();
      data.forEach(function(song) {
        if(lists[song.playlistId] == null) {
          lists[song.playlistId] = [song];
        } else {
          lists[song.playlistId].push(song);
        }
      });

      var fileContent = ''
      for(var key in lists) {
        fileContent += 'Playlist ' + (parseInt(key)+1) + '\n';
        fileContent += 'Tags: ' + playlistsTags[key].toString().replaceAll(',', ' ') + '\n';
        lists[key].forEach(function(song) {
          fileContent += song.artist + '\t\t' + song.title + '\n'
        })
        fileContent += '\n'
      }
      var blob = new Blob([fileContent], {type: "text/plain;charset=utf-8"});
      saveAs(blob, filename+'.txt');
    }

    return chart;
}
