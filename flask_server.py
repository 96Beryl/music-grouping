from flask import Flask, request, make_response, jsonify
from config import FLASK_SECRET
from flask_cors import CORS
import grouping

app = Flask(__name__, static_folder='static')
app.config['SECRET_KEY'] = FLASK_SECRET
CORS(app)


@app.route('/', methods=['GET'])
def index():
    return app.send_static_file('index.html')


@app.route('/playlists/<string:username>/<int:n_songs>', methods=['GET'])
def get_playlists(username, n_songs):
    t = grouping.get_unique_recent_tracks(username, n_songs)
    df = grouping.get_acousticbrainz_data(t)
    classified = grouping.classify(grouping.drop_correlated(df, threshold=0.6), pca_comp=25)
    tags = grouping.get_tags(classified)
    output = grouping.get_playlists(classified, tags)
    return make_response(jsonify(playlists=output), 200)


if __name__ == '__main__':
    app.run()
